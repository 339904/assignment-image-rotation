#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_image.h"

struct image rotate_image_90L( struct image const source )
{
	struct image img_rotated = image_create(source.height, source.width);

	for ( size_t i = 0; i != source.width; i++ )
	{
		for ( size_t j = 0; j != source.height; j++ )
		{
			img_rotated.data[i * img_rotated.width + img_rotated.width - j - 1] = source.data[source.width * j + i];
		}
	}

	return img_rotated;
}

//can add new foo here
