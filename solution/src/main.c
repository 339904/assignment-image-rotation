#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_image.h"
#include "foo_for_bmp.h"


int main( int argc, char** argv )
{
    (void) argc; (void) argv; // supress 'unused parameters' warning

    //open fiels
    FILE *file_in   = fopen(argv[1], "rb");
    FILE *file_out  = fopen(argv[2], "wb");

    struct image img_source;
    struct image img_result;

    //magic
    from_bmp(file_in, &img_source);
    img_result = rotate_image_90L(img_source);
    to_bmp(file_out, &img_result);

    //close files, good job little programm
    free(img_source.data);
    free(img_result.data);

    fclose(file_in);
    fclose(file_out);
    return 0;
}
