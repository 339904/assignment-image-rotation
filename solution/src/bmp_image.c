#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_image.h"

uint32_t size_image (struct image source)
{
	return source.width * source.height * sizeof(struct pixel);
}

struct image image_create(uint64_t width, uint64_t height)
{
	return (struct image)
	{
		.width = width,
		.height = height,
		.data = malloc(width * height * sizeof(struct pixel))
	};
}

struct bmp_header bmp_header_create_from_image(struct image source)
{
  return (struct bmp_header)
  {
    .bfType = 0x4D42,
    .bfileSize = sizeof(struct bmp_header) * size_image(source),
    .bfReserved = 0,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = 40,
    .biWidth = source.width,
    .biHeight = source.height,
    .biPlanes = 1,
    .biBitCount = 24,
    .biCompression = 0,
    .biSizeImage = size_image(source),
    .biXPelsPerMeter = 0,
    .biYPelsPerMeter = 0,
    .biClrUsed = 0,
    .biClrImportant = 0,
  };
}

enum read_status from_bmp( FILE* file_in, struct image* img_source )
{
		struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, file_in);

		*img_source = image_create(header.biWidth, header.biHeight);

		for ( size_t i = 0; i != header.biHeight; i++ )
		{
      fread((*img_source).data + (i * header.biWidth), sizeof(struct pixel) * header.biWidth, 1, file_in);
			fseek(file_in, (uint8_t) (*img_source).width % 4, SEEK_CUR);
		}
		return 0;
}

enum write_status to_bmp( FILE* file_out, struct image const* img_result )
{
  		struct bmp_header header = bmp_header_create_from_image(*img_result);
  		fwrite(&header, sizeof(struct bmp_header), 1, file_out);

  		uint8_t padding[4] = {0,0,0,0};

  		for ( size_t i = 0; i != header.biHeight; i++ )
  		{
				fwrite((*img_result).data + (i * header.biWidth), sizeof(struct pixel) * header.biWidth, 1, file_out);
				fwrite(padding, header.biWidth % 4, 1, file_out);
  		}
  		return 0;
}
